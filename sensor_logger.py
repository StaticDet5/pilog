#!/usr/bin/python

import time
import logging
from Ty_BMP085 import BMPsensor

def sensor_logger():
    TPA = BMPsensor()
    htmlfile = '/var/www/index.html'
    logging.basicConfig(
        filename='sensor_log.log',
        format= '%(levelname) -10s %(created)0.1f %(message)s',
        level= logging.INFO
        )
    
    if TPA[0] < 30:
        logging.info(TPA[0:2])
        webpage = '''
    <html><body><h1>It works!</h1>
<p>This is the default web page for this server.</p>
<p>The web server software is running but no content has been added, yet.</p>
<p>DET5 Rocks!</p>

<p>The temp is: ''' + str(TPA[0]) + '''
</p>
<p>The pressure is: ''' + str(TPA[1]) + '''
</p>
<p>At: ''' + time.asctime() + '''
</p>
</body></html>'''
    if TPA[0] > 30:
        logging.warning(TPA[0:2])
        webpage = '''
    <html><body><h1>It works!</h1>
<p>This is the default web page for this server.</p>
<p>The web server software is running but no content has been added, yet.</p>
<p>DET5 Rocks!</p>

<p>The temp is: ''' + str(TPA[0]) + ''' TOO HOT!!!
</p>
<p>The pressure is: ''' + str(TPA[1]) + '''
</p>
<p>At: ''' + time.asctime() + '''
</p>
</body></html>'''
    
    open(htmlfile, 'w').write(webpage)
    
